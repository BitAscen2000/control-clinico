$(document).ready(inicio);

//implementacion de las funcoines
function inicio() {
    cargarDatos();
    $("#guardarPaci").click(guardar);
    $("#modificarPaci").click(modificar);
    $("#eliminarPaciente").click(eliminar);
}

//function pára limpiar los modal
function reset() {
    $("#id").val(null);
    $("#nombre").val(null);
    $("#direccion").val(null);
}

//function para cargar los json 
function cargarDatos() {
    $.ajax({
        method: "Get",
        url: "http://localhost:8080/paciente/all",
        data: null,
        success:cargarTabla,
        error:errorFunction
    });
}

//function para cargar los datos en la tabla
function cargarTabla(response) {
    $("#tdatos").html("");
    for(value of response){
        $("#tdatos").append(""
            +"<tr>"
                +"<td>"+value.id_paciente+"</td>"
                +"<td>"+value.nombre+"</td>"
                +"<td>"+value.direccion+"</td>"
                +"<td>"
                +"<button onclick='cargarRegistro("+value.id_paciente+")' type='button' class='btn btn-primary mr-2' data-toggle='modal' data-target='#ModalMod'>editar</button>"
                +"<button onclick='dropId("+value.id_paciente+")' type='button' class='btn btn-danger' data-toggle='modal' data-target='#ModalDrop'>eliminar</button>"
                +"</td>"
            +"</tr>"
        +"");
    }
}

//function para error
function errorFunction(response) {
    console.log(response.mensaje);
}

//function para guardar
function guardar() {
    $.ajax({
        url:"http://localhost:8080/paciente/save",
        method:"Get",
        data:{
            nombre:$("#nombre").val(),
            direccion:$("#direccion").val()
        },
        success:function (response) {
            reset();
            cargarDatos();
          },
        error:errorFunction
    });
}

//function para cargar modal modificar
function cargarRegistro(id_paciente) {
    $.ajax({
        url:"http://localhost:8080/paciente/getPaciente/"+id_paciente,
        method:"Get",
        data:null,
        success:function (response) {
            $("#id2").val(response.id_paciente);
            $("#nombre2").val(response.nombre);
            $("#direccion2").val(response.direccion);
          },
        error:errorFunction
    });
}

//functio para editar
function modificar() {
    var id_paciente= $("#id2").val();
    $.ajax({
        url:"http://localhost:8080/paciente/update/"+id_paciente,
        method:"Get",
        data:{
            id_paciente:id_paciente,
            nombre:$("#nombre2").val(),
            direccion:$("#direccion2").val()
        },
        success:function (response) {
            cargarDatos();
          },
        error:errorFunction
    });
}

//function para cargar modal eliminar
function dropId(id_paciente) {
    $("#idPaci").val(id_paciente);
}

//function para eliminar
function eliminar() {
    var id_paciente=$("#idPaci").val();  
    $.ajax({
        url:"http://localhost:8080/paciente/delete/"+id_paciente,
        method:"Get",
        data:null,

        success:function (response) { 
            cargarDatos();
         },
         error:errorFunction
    });
}
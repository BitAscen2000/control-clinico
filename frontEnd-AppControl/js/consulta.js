$(document).ready(inicio);
//implementacion de las funciones
function inicio() {
    cargarDatos();
    cargarNeurologo();
    cargarPaciente();
    $("#guardarCon").click(guardar);
    $("#modificarCon").click(modificar);
    $("#eliminarConsulta").click(eliminar);
}

//function para resetear los modal
function reset() {
    //reset modal guardar
    $("#id").val(null);
    $("#fecha").val(null);
    $("#sintoma").val(null);
    $("#diagnostico").val(null);
    $("#neurologo").val(null);
    $("#paciente").val(null);
}

//funcion para cargar los datos
function cargarDatos() {
    $("#ScripTabla").DataTable({}); 
    $.ajax({
        type: "Get",
        url: "http://localhost:8080/consulta/all",
        data: "data",
        success: cargarTabla,
        error:errorFunction
    });
}

//function para listar en tabla
function cargarTabla(response) {
    $("#tdatos").html("");
    //recorrer los datos
    for(let item of response){
        $("#tdatos").append(""
            +"<tr>"
                +"<td>"+item.id_consulta+"</td>"
                +"<td>"+item.fecha+"</td>"
                +"<td>"+item.sintoma+"</td>"
                +"<td>"+item.diagnostico+"</td>"
                +"<td>"+item.neurologo.nombre+"</td>"
                +"<td>"+item.paciente.nombre+"</td>"
                +"<td>"
                + "<button onclick='selecRegistro("+item.id_consulta+")' type='button' class='btn btn-primary mr-2' data-toggle='modal' data-target='#ModalMod'>Editar</button>"
                + "<button onclick='dropId("+item.id_consulta+")' type='button' class='btn btn-danger' data-toggle='modal' data-target='#ModalDrop'>Eliminar</button>"                +"</td>"
            +"<tr>"
        +"");
    }
}

//function para el error
function errorFunction(response) { 
    console.log(response.mensaje);
 }

//function para cargar select en modal
function cargarNeurologo(){
    $.ajax({
        type: "Get",
        url: "http://localhost:8080/consulta/allNeurologo",
        data: "data",
        success: function (response) {
         for(item of response){
             $("#neurologo").append(""
                +"<option value='"+item.id_neurologo+"'>"+"Nombre: "+item.nombre+" -- Especialidad: "+item.especialidad.especialidad+"</option>"
             +"");

             $("#neurologo2").append(""
             +"<option value='"+item.id_neurologo+"'>"+"Nombre: "+item.nombre+" -- Especialidad: "+item.especialidad.especialidad+"</option>"
          +"");
         }  
        },
        error:errorFunction
    });
}

//function para cargar select en modal
function cargarPaciente() {
    $.ajax({
        url:"http://localhost:8080/consulta/allPaciente",
        method:"Get",
        data:"data",
        success:function (response) {
            for(item of response){
                //selelct modal guadar
                $("#paciente").append(""
                +"<option value='"+item.id_paciente+"'>"+"Nombre: "+item.nombre+" -- Direccion: "+item.direccion+"</option>"+"");
                //select modal modificar
                $("#paciente2").append(""
                +"<option value='"+item.id_paciente+"'>"+"Nombre: "+item.nombre+" -- Direccion: "+item.direccion+"</option>"+"");
            }
        },
        error:errorFunction
    });
}

 //funcion para guardar
 function guardar() {
     $.ajax({
         url:"http://localhost:8080/consulta/save",
         method:"Get",
         data:{
             fecha:$("#fecha").val(),
             sintoma:$("#sintoma").val(),
             diagnostico:$("#diagnostico").val(),
             id_neurologo:$("#neurologo").val(),
             id_paciente:$("#paciente").val()
         },
         success:function (response) {
             reset();
             cargarDatos();
         },
         error:errorFunction
     });
 }

 //function para cargar el modal modificar
 function selecRegistro(id_consulta) {
    $.ajax({
        url:"http://localhost:8080/consulta/getConsulta/"+id_consulta,
        method:"Get",
        data:null,

        success: function (response) {
            $("#id2").val(response.id_consulta);
            $("#fecha2").val(response.fecha);
            $("#sintoma2").val(response.sintoma);
            $("#diagnostico2").val(response.diagnostico);
            $("#neurologo2").val(response.neurologo.id_neurologo);
            $("#paciente2").val(response.paciente.id_paciente);
          },
        error:errorFunction
    });
 }

 //function para modificar
 function modificar() {
     var id_consulta=$("#id2").val();
     $.ajax({
         method: "Get",
         url: "http://localhost:8080/consulta/update/"+id_consulta,
         data: {
             id_consulta:id_consulta,
             fecha:$("#fecha2").val(),
             sintoma:$("#sintoma2").val(),
             diagnostico:$("#diagnostico2").val(),
             id_neurologo:$("#neurologo2").val(),
             id_paciente:$("#paciente2").val()
         },
         success: function (response) {
             cargarDatos();
         },
         error:errorFunction
     });
 }

 //function para cargar id en modal eliminar
 function dropId(id_consulta) {
    $("#idConsul").val(id_consulta);
 }

 function eliminar() {
     var id_consulta=$("#idConsul").val();
     $.ajax({
         url:"http://localhost:8080/consulta/delete/"+id_consulta,
         method:"Get",
         data:null,

         success:function (response) { 
             cargarDatos();
          },
          error:errorFunction
     });
 }
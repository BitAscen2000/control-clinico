let neurologo ={id_neurologo:0,nombre:""};
let paciente ={id_paciente:0,nombre:""};

$(document).ready(inicio);

//implementacion de todas las funciones
function inicio() {
    cargarPaciente();
    cargarNeurologo();
    $("#agregarConsulta").click(agregarDetalle);
    cargarDetalle();
    //resetDetalles();
    $("body").on('click','.agregarNeu',function () {
        mostrarNeurologo($(this).parent().parent().children('td:eq(0)').text(),$(this).parent().parent().children('td:eq(1)').text());
      });
    $("body").on('click','.agregarPaci',function () {
        mostrarPaciente($(this).parent().parent().children('td:eq(0)').text(),$(this).parent().parent().children('td:eq(1)').text());
      });
}

//function para mostra info en input
function mostrarNeurologo(id_neurologo,nombre){
    neurologo.id_neurologo=id_neurologo;
    neurologo.nombre=nombre;
    $("#neurologo").val(neurologo.nombre);
}

//function para mostra info en input
function mostrarPaciente(id_paciente,nombre) {
    paciente.id_paciente=id_paciente;
    paciente.nombre=nombre;
    $("#paciente").val(paciente.nombre);
}


//function para cargar la tabla de paciente
function cargarPaciente() {
    $("#TablaPaci").DataTable({
        "ajax":{
            "method" : "Get",
            "url" : "http://localhost:8080/consulta/allPaciente",
        },

        "columns":[{
            "data" : "id_paciente",
            "width" : "20%" 
        },{
            "data" : "nombre",
            "width" : "20%" 
        },{
            "data" : "direccion",
            "width" : "20%" 
        },{
            "data" : "operaciones",
            "width" : "40%" 
        }],

        "language" : {
            "lengthMenu" : "Mostrar _MENU_ en tabla",
            "zeroRecords" : "Datos no encontrados",
            "info" : "Mostar páginas _PAGE_ de _PAGES_",
            "infoEmpty" : "Datos no encontrados",
            "infoFiltered" : "(Filtrados por _MAX_ total registros)",
            "search" : "Buscar:",
            "paginate" : {
                "first" : "Primero",
                "last" : "Anterior",
                "next" : "Siguiente",
                "previous" : "Anterior"
            },
        }
    });
}

//function para cargar la tabla de doctores
function cargarNeurologo() {
    $("#TablaNeu").DataTable({
        "ajax":{
            "method" : "Get",
            "url" : "http://localhost:8080/consulta/allNeurologo",
        },

        "columns" :[{
            "data" : "id_neurologo"
        },{
            "data" : "nombre"
        },{
            "data" : "direccion"
        },{
            "data" : "especialidad"
        },{
            "data" : "operaciones"
        }],

        "language" : {
            "lengthMenu" : "Mostrar _MENU_ ",
            "zeroRecords" : "Datos no encontrados",
            "info" : "Mostar páginas _PAGE_ de _PAGES_",
            "infoEmpty" : "Datos no encontrados",
            "infoFiltered" : "(Filtrados por _MAX_ total registros)",
            "search" : "Buscar:",
            "paginate" : {
                "first" : "Primero",
                "last" : "Anterior",
                "next" : "Siguiente",
                "previous" : "Anterior"
            },
        }
    });
}

//function para ver info en input
function inputNeu(id_neurologo,nombre) {
    $("#doctor").val(nombre);
}

//function para ver info en el input
function inputPaci(id_paciente,nombre) {
    $("#paciente").val(nombre);
}

//function para resetear detalles
function resetDetalles() {
    $.ajax({
        type: "Post",
        url: "http://localhost:8080/consulta/resetDetalles",
    });
}

//function para agregar detalles
function agregarDetalle() {
    $.ajax({
        method: "Post",
        url: "http://localhost:8080/consulta/agregarDetalle",
        data: {
            descripcion:$("#descripcion").val()
        },
        success: function (response) {
            console.log(response);
            cargarDetalle();
            $("#descripcion").val("");
        },
        error:errorFunction
    });
}
//function para los errores
function errorFunction(response) {
    console.log("error en la peticion");
}

function cargarDetalle() {
    $.ajax({
        method:"Get",
        url:"http://localhost:8080/consulta/detalles",
        
        success: function (response) {
            $("#tdatosConsulta").html("");
            for(value of response){
                $("#tdatosConsulta").append(""
                    +"<tr>"
                    +"<td>"+value.descripcion+"</td>"
                    +"<td><button class='btn btn-danger'>eliminar</button></td>"
                    +"</tr>"
                +"");
            }
          },
          error:errorFunction
    });
}

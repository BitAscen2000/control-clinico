$(document).ready(inicio);
//implementacion de las funciones
function inicio(){
    cargarDatos();
    cargarEspecialidad();
    $("#guardarNeu").click(guardar);
    $("#eliminarNeurologo").click(eliminar);
    $("#modificarNeu").click(modificar);
}
//funciones para limpiar los modal
function reset(){
    $("#nombre").val(null);
    $("#direccion").val(null);
    $("#especialidad").val(null);
}

//traer los datos de la base atraves del controlador
function cargarDatos() {
    $.ajax({
        type: "Get",
        url: "http://localhost:8080/neurologo/all",
        data: null,
        success: cargarTabla,
        error: errorPeticion
    });
}

//insertarlo en la tabla
function cargarTabla(response) {
    $("#tdatos").html("");
    for(let value of response){
        $("#tdatos").append(""
        +"<tr>"
            +"<td>"+value.id_neurologo+"</td>"
            +"<td>"+value.nombre+"</td>"
            +"<td>"+value.direccion+"</td>"
            +"<td>"+value.especialidad.especialidad+"</td>"
            +"<td>"
                + "<button onclick='selecRegistro("+value.id_neurologo+")' type='button' class='btn btn-primary mr-2' data-toggle='modal' data-target='#ModalMod'>Editar</button>"
                + "<button onclick='dropId("+value.id_neurologo+")' type='button' class='btn btn-danger' data-toggle='modal' data-target='#ModalDrop'>Eliminar</button>"
            +"</td>"
        +"</tr>");
    }
}

//funcion de error
function errorPeticion(response) {
    console.log("error en la peticion: "+response);
}

//funcion para cargar las especialidades
function cargarEspecialidad() {
    $.ajax({
        url:"http://localhost:8080/neurologo/allEspecialidad",
        method:"Get",
        data:null,
        success:function (response){
            response.forEach(item => {
                $("#especialidad").append(""
                +"<option value='"+item.id+"'>"+item.especialidad+"</option>"+
                "");

                $("#especialidad2").append(""
                +"<option value='"+item.id+"'>"+item.especialidad+"</option>"+
                "");
            });
        }
    });
}

//funcion para guardar
function guardar() {
    $.ajax({
        url:"http://localhost:8080/neurologo/save",
        method:"Get",
        data:{
            nombre:$("#nombre").val(),
            direccion:$("#direccion").val(),
            id:$("#especialidad").val()
        },
        success:function (response) {
            reset();
            cargarDatos();
        },
        error:errorPeticion
    });
}

//funcion para cargar en modal modificar
function selecRegistro(id_neurologo){
    $.ajax({
        url:"http://localhost:8080/neurologo/GetNeurologo/"+id_neurologo,
        method:"Get",
        data:null,
        success:function (response) {
          $("#id2").val(response.id_neurologo);
          $("#nombre2").val(response.nombre);
          $("#direccion2").val(response.direccion);
          $("#especialidad2").val(response.especialidad.id);  
        },
        error:errorPeticion
    });
}

//funcion para modificar
function modificar() {
    var id_neurologo=$("#id2").val();
    $.ajax({
        url:"http://localhost:8080/neurologo/update/"+id_neurologo,
        method:"Get",
        data:{
            id_neurologo:id_neurologo,
            nombre:$("#nombre2").val(),
            direccion:$("#direccion2").val(),
            id:$("#especialidad2").val()
        },
        success:function (response) {
            //alert(response.mensaje);
            cargarDatos();
        },
        error:errorPeticion
    });
}

//funcion para cargar el id en modal de eliminar
function dropId(id_neurologo){
    $("#idNeurologo").val(id_neurologo);
}

//funcion para eliminar el registro
function eliminar() {
    var id_neurologo=$("#idNeurologo").val();
    $.ajax({
        url:"http://localhost:8080/neurologo/delete/"+id_neurologo,
        method:"Get",
        data:null,

        success: function (response) {
            //alert(response.mensaje);
            cargarDatos();
        },
        error:errorPeticion
    });
}

$(document).ready(inicio);

//implementaciones de las funciones
function inicio() {
    cargarDatos();
    $("#guardarEsp").click(guardar);
    $("#eliminarEspecial").click(eliminar);
    $("#modificarEsp").click(modificar);
}

//function para limpiar los modal
function reset() {
    $("#id").val(null);
    $("#especialidad").val(null);
}

//cargar los json 
function cargarDatos() {
    $.ajax({
        url: "http://localhost:8080/especialidad/all",
        method:"Get",
        data: "data",
        success:cargarTabla,
        error:errorFunction
    });
}

//cargar la tabla con los datos
function cargarTabla(response) {
    $("#tdatos").html("");
    for(item of response){
        $("#tdatos").append(""
            +"<tr>"
                +"<td>"+"</td>"
                +"<td>"+item.id+"</td>"
                +"<td>"+item.especialidad+"</td>"
                +"<td>"
                +"<button onclick='cargarRegistro("+item.id+")' type='button' class='btn btn-primary mr-2' data-toggle='modal' data-target='#ModalMod'>editar</button>"
                +"<button onclick='dropId("+item.id+")' type='button' class='btn btn-danger' data-toggle='modal' data-target='#ModalDrop'>eliminar</button>"
                +"</td>"
                +"<td>"+"</td>"
            +"</tr>"
        +"");
    }
}

//function para el error
function errorFunction(response) {
    console.log(response.mensaje);
}

//function para guardar
function guardar() {
    $.ajax({
        url:"http://localhost:8080/especialidad/save",
        method:"Get",
        data:{
            especialidad:$("#especialidad").val()
        },
        success: function (response) {
            reset();
            cargarDatos();
        },
        error:errorFunction
    });
}

//function para modificar
function cargarRegistro(id){
    $.ajax({
        url:"http://localhost:8080/especialidad/getEspecialidad/"+id,
        method:"Get",
        data:null,

        success: function (response) {
            $("#id2").val(response.id);
            $("#especialidad2").val(response.especialidad);
        },
        error:errorFunction
    });
}

//function para modificar
function modificar(){
    var id = $("#id2").val();
    $.ajax({
        url:"http://localhost:8080/especialidad/update/"+id,
        method:"Get",
        data:{
            id:id,
            especialidad:$("#especialidad2").val()
        },
        success: function(){
            cargarDatos();
        }, 
        error:errorFunction
    });
}
//functoin para eliminar
function dropId(id){
    $("#idPaci").val(id);
}

//function para eliminar
function eliminar(){
    var id=$("#idPaci").val();
    $.ajax({
        url:"http://localhost:8080/especialidad/delete/"+id,
        method:"Get",
        data:null,

        success:function (response) {
            cargarDatos();
        }, error:errorFunction
    });
}
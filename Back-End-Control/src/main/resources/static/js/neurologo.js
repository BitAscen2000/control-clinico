inicio();

function inicio(){
	$.ajax({
		url:"http://localhost:8080/neurologo/all",
	    method:"Get",
	    
	    success: function(response){
	    	$("#tdatos").html("");
	    	for(let n=0; n < response.length; n++){
	    		$("#tdatos").append(""
	    				+"<tr>"
	    					+"<td>"+response[n].id_neurologo+"</td>"
	    					+"<td>"+response[n].nombre+"</td>"
	    					+"<td>"+response[n].direccion+"</td>"
	    					+"<td>"+response[n].especialidad.especialidad+"</td>"
	    					+"<td>"
	    					+ "<button  type='button' class='btn btn-success mr-2'>Editar</button>"
	    					+ "<button type='button' class='btn btn-danger'>Eliminar</button>"
	    					+"</td>"
	    				+"</tr>");
	    	}
	    },
	    error: function (response) {
			console.log("Error en la peticion");
		}
	});
	}
inicio();

function inicio(){
	$.ajax({
		url:"/paciente/all",
		method: "Get",
		
		success: function (response){
			$("#tdatos").html("");
			for(let p=0; p < response.length; p++){
				$("#tdatos").append(""
						+"<tr>"
						 +"<td>"+response[p].id_paciente+"</td>"
						 +"<td>"+response[p].nombre+"</td>"
						 +"<td>"+response[p].direccion+"</td>"
						 +"<td>"
						 + "<button  type='button' class='btn btn-success mr-2'>Editar</button>"
	    				 + "<button type='button' class='btn btn-danger'>Eliminar</button>"
						 +"</td>"
						+"</tr>");
			}
		},
		error: function (response){
			console.log("error en la peticion");
		}
	});
}
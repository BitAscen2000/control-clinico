package com.cds.crud.service;

import java.util.*;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.cds.crud.entity.Especialidad;
import com.cds.crud.entity.Neurologo;
import com.cds.crud.repository.IEspecialidadRepository;
import com.cds.crud.repository.INeurologoRepository;

@Service
public class NeurologoService {
	//metodo para sobre escribir datos 
	@Autowired
	INeurologoRepository erNeurologo;
	@Autowired
	IEspecialidadRepository respecialidad;
	
	//metodo para listar
	@Transactional
	public List<Neurologo> getAllNeurologos(){
		return ( List<Neurologo>) erNeurologo.findAll();	
	}
	
	//metodo para listar
		@Transactional
		public List<Especialidad> getAllEspecialidad(){
			return ( List<Especialidad>) respecialidad.findAll();	
		}
	
	//metodo para guardar y modificar
	@Transactional
	public Boolean SaveOrUpdate(Neurologo entity) {
		try {
			erNeurologo.save(entity);
			
			return true;
		}catch (Exception e) {
			return false;
		}
	}
	//metodo para eliminar
	@Transactional
	public Boolean delete(Neurologo entity) {
		try {
			erNeurologo.delete(entity);
			
			return true;
		}catch(Exception e) {
			return false;
		}
	}
	//metodo para obener los datos
	@Transactional
	public Especialidad getEspecialidad(Integer id) {
		return respecialidad.findById(id).get();
	}
	
	@Transactional
	public Neurologo getNeurologo(Integer id_neurologo) {
		return erNeurologo.findById(id_neurologo).get();
	}

}

package com.cds.crud.service;

import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.cds.crud.entity.Consulta;
import com.cds.crud.entity.Especialidad;
import com.cds.crud.entity.Neurologo;
import com.cds.crud.entity.Paciente;
import com.cds.crud.repository.IConsultaRepository;
import com.cds.crud.repository.IEspecialidadRepository;
import com.cds.crud.repository.INeurologoRepository;
import com.cds.crud.repository.IPacienteRepository;

@Service
public class ConsultaService {

	@Autowired
	IConsultaRepository reConsulta;

	@Autowired
	INeurologoRepository reNeurologo;

	@Autowired
	IPacienteRepository rePaciente;

	@Autowired
	IEspecialidadRepository respecialidad;

	// Metodo para obtener las listar json de consultas
	@Transactional
	public List<Consulta> getAllConsultas() {
		return (List<Consulta>) reConsulta.findAll();
	}

	// metodo para listar json de neurologo
	@Transactional
	public List<Neurologo> getAllNeurologos() {
		return (List<Neurologo>) reNeurologo.findAll();
	}

	// metodo para listar los json de pacientes
	@Transactional
	public List<Paciente> getAllPacientes() {
		return (List<Paciente>) rePaciente.findAll();
	}

	// metodo para listar
	@Transactional
	public List<Especialidad> getAllEspecialidad() {
		return (List<Especialidad>) respecialidad.findAll();
	}

	// metodo para guardar y modificar
	@Transactional
	public Boolean SaveorUpdate(Consulta entity) {
		try {
			reConsulta.save(entity);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	// metodo para eliminar
	@Transactional
	public Boolean delete(Consulta entity) {
		try {
			reConsulta.delete(entity);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	// metodo para obtener los datos
	@Transactional
	public Consulta getConsulta(Integer id_consulta) {
		return reConsulta.findById(id_consulta).get();
	}

	@Transactional
	public Neurologo getNeurologo(Integer id_neurologo) {
		return reNeurologo.findById(id_neurologo).get();
	}

	@Transactional
	public Paciente getPaciente(Integer id_paciente) {
		return rePaciente.findById(id_paciente).get();
	}

	// metodo para obener los datos
	@Transactional
	public Especialidad getEspecialidad(Integer id) {
		return respecialidad.findById(id).get();
	}
}

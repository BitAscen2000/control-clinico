package com.cds.crud.entity;

import java.sql.Date;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.ManyToOne;


@Entity
@Table(name = "consulta")
public class Consulta {
	//campos de la tabla 
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id_consulta;
	private Date fecha;
	private String diagnostico;
	
	//relacion muchos a uno
	@ManyToOne(optional = false, fetch = FetchType.EAGER)
	private Neurologo neurologo;
	
	@ManyToOne(optional = false, fetch = FetchType.EAGER)
	private Paciente paciente;
	//contructores
	public Consulta() {
		super();
	}

	public Consulta(Integer id_consulta, Date fecha, String diagnostico, Neurologo neurologo,Paciente paciente) {
		super();
		this.id_consulta = id_consulta;
		this.fecha = fecha;
		this.diagnostico = diagnostico;
		this.neurologo = neurologo;
		this.paciente = paciente;
	}
	
	//Getters and Setters
	public Integer getId_consulta() {
		return id_consulta;
	}

	public void setId_consulta(Integer id_consulta) {
		this.id_consulta = id_consulta;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getDiagnostico() {
		return diagnostico;
	}

	public void setDiagnostico(String diagnostico) {
		this.diagnostico = diagnostico;
	}

	public Neurologo getNeurologo() {
		return neurologo;
	}

	public void setNeurologo(Neurologo neurologo) {
		this.neurologo = neurologo;
	}

	public Paciente getPaciente() {
		return paciente;
	}

	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}
	
}

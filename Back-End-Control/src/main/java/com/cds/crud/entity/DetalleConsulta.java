package com.cds.crud.entity;

import javax.persistence.*;


@Entity
@Table(name="detalle_consulta")
public class DetalleConsulta {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id_detalle; 
	
	private String descripcion;
	
	@ManyToOne(optional = false, fetch = FetchType.EAGER)
	private Consulta consulta;
	
	//cosntructores
	public DetalleConsulta() {
		
	}

	public DetalleConsulta(Integer id_detalle, String descripcion, Consulta consulta) {
		super();
		this.id_detalle = id_detalle;
		this.descripcion = descripcion;
		this.consulta = consulta;
	}

	//Getters and setters
	public Integer getId_detalle() {
		return id_detalle;
	}

	public void setId_detalle(Integer id_detalle) {
		this.id_detalle = id_detalle;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Consulta getConsulta() {
		return consulta;
	}

	public void setConsulta(Consulta consulta) {
		this.consulta = consulta;
	}
}

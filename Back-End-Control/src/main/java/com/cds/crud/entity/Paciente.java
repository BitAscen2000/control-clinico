package com.cds.crud.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name= "paciente")
public class Paciente {
	//campos de la tabla
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id_paciente;
	private String nombre;
	private String direccion;
	//cosntructores
	public Paciente() {
		super();
	}
	public Paciente(Integer id_paciente, String nombre, String direccion) {
		super();
		this.id_paciente = id_paciente;
		this.nombre = nombre;
		this.direccion = direccion;
	}
	//Getters and Setters
	public Integer getId_paciente() {
		return id_paciente;
	}
	public void setId_paciente(Integer id_paciente) {
		this.id_paciente = id_paciente;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	
}

package com.cds.crud.entity;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="neurologo")
public class Neurologo {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id_neurologo;
	private String nombre;
	private String direccion;
	
	@ManyToOne(optional = false, fetch =FetchType.EAGER)
	@NotNull
	private Especialidad especialidad;
	
	//constructores
	public Neurologo() {
		super();
	}

	public Neurologo(Integer id_neurologo, String nombre, String direccion, Especialidad especilidad) {
		super();
		this.id_neurologo = id_neurologo;
		this.nombre = nombre;
		this.direccion = direccion;
		this.especialidad = especilidad;
	}

	public Integer getId_neurologo() {
		return id_neurologo;
	}

	public void setId_neurologo(Integer id_neurologo) {
		this.id_neurologo = id_neurologo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public Especialidad getEspecialidad() {
		return especialidad;
	}

	public void setEspecialidad(Especialidad especialidad) {
		this.especialidad = especialidad;
	}
}

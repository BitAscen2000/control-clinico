package com.cds.crud.controller;

import java.util.HashMap;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.cds.crud.entity.Paciente;
import com.cds.crud.repository.IPacienteRepository;
import com.cds.crud.service.ConsultaService;

@Controller
@CrossOrigin
@RequestMapping("paciente")
public class PacienteController {

	@Autowired
	IPacienteRepository rePaciente;
	
	@Autowired
	ConsultaService daoConsulta;

	//metodo para llamar vista
	@GetMapping(value="index")
	public String paciente() {
		return "views/paciente/indexP";
	}
	
	//listar 
	@GetMapping(value="all",produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<Paciente> getAll(){
		return (List<Paciente>) rePaciente.findAll();
	} 
	//guardar
	@GetMapping(value="save")
	@ResponseBody
	public HashMap<String,String> save(
			@RequestParam String nombre,
			@RequestParam String direccion){
		
		Paciente p = new Paciente();//se crea un nuevo objeto
		
		 HashMap<String, String> jsonReturn=new HashMap<>();
		 //asignado datos al objeto
		 p.setNombre(nombre);
		 p.setDireccion(direccion);
		 
		 try {
			 rePaciente.save(p);
			 
			 jsonReturn.put("estado", "OK");
			 jsonReturn.put("mensaje", "Registro guardado");
			 
			 return jsonReturn;
		 }catch(Exception e) {
			 jsonReturn.put("estado", "ERROR");
			 jsonReturn.put("mensaje", "Registro No guardado");
			 
			 return jsonReturn;
		 }
	}
	//listar registro
	@GetMapping(value = "getPaciente/{id_paciente}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Paciente getPaciente(@PathVariable Integer id_paciente) {
		return daoConsulta.getPaciente(id_paciente);
	}
	//eliminar
	@GetMapping("delete/{id_paciente}")
	@ResponseBody
	public HashMap<String, String> eliminar(@PathVariable Integer id_paciente) {
		
		HashMap<String, String> jsonReturn=new HashMap<>();
		
		try {
			Paciente p = rePaciente.findById(id_paciente).get();
			
			rePaciente.delete(p);
			 jsonReturn.put("estado", "OK");
			 jsonReturn.put("mensaje", "Registro guardado");
			 
			 return jsonReturn;
		}catch(Exception e) {
			 jsonReturn.put("estado", "ERROR");
			 jsonReturn.put("mensaje", "Registro No guardado");
			 
			 return jsonReturn;
		}
	}
	//update
	@GetMapping(value="update/{id_paciente}")
	@ResponseBody
	public HashMap<String,String> update(
			@RequestParam Integer id_paciente,
			@RequestParam String nombre,
			@RequestParam String direccion){
		
		Paciente p = new Paciente();//se crea un nuevo objeto
		
		 HashMap<String, String> jsonReturn=new HashMap<>();
		 //asignado datos al objeto
		 p.setNombre(nombre);
		 p.setDireccion(direccion);
		 p.setId_paciente(id_paciente);
		 
		 try {
			 rePaciente.save(p);
			 
			 jsonReturn.put("estado", "OK");
			 jsonReturn.put("mensaje", "Registro guardado");
			 
			 return jsonReturn;
		 }catch(Exception e) {
			 jsonReturn.put("estado", "ERROR");
			 jsonReturn.put("mensaje", "Registro No guardado");
			 
			 return jsonReturn;
		 }
	}
	
}

package com.cds.crud.controller;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.cds.crud.entity.Consulta;
import com.cds.crud.entity.DetalleConsulta;
import com.cds.crud.entity.Especialidad;
import com.cds.crud.entity.Neurologo;
import com.cds.crud.entity.Paciente;
import com.cds.crud.service.ConsultaService;
import com.cds.crud.service.NeurologoService;

@Controller
@CrossOrigin
@RequestMapping("consulta")
public class ConsultaController {
	// persistencia o manejo de datos
	@Autowired
	ConsultaService daoConsulta;


	public static List<DetalleConsulta> detalles = new ArrayList<>();

	// constructor para la lista estatica
	public ConsultaController() {
		detalles = new ArrayList<>();
	}

	// metodo para agregar detalles
	@PostMapping(value = "agregarDetalle", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Object agregarDetalle(@RequestParam String descripcion) {
		DetalleConsulta entity = new DetalleConsulta();
		entity.setDescripcion(descripcion);

		detalles.add(entity);
		HashMap<String, String> json = new HashMap<String, String>();
		json.put("estado", "OK");
		json.put("mensaje", "Registro Guardado");
		return json;
	}

	// retorno de la lista de detalles
	@GetMapping(value = "detalles", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Object getDetalles() {
		return detalles;
	}

	// Reseteo de la cache
	@PostMapping(value = "resetDetalles", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Object resetDetalles() {
		detalles = new ArrayList<>();
		return "Lista detalles reset";
	}
	
	//eliminar detalles
	@GetMapping(value = "eliminarDetalle", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Object eliminar() {
		
		return "Dato eliminado";
	}

	// metodo para llamar vista
	@GetMapping(value = "index")
	public String consulta() {
		return "views/consulta/indexC";
	}

	// metodo para listar
	@GetMapping(value = "all", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<Consulta> getAllConsultas() {
		return (List<Consulta>) daoConsulta.getAllConsultas();
	}

	// metodo para traer los neurologos
	@GetMapping(value = "allNeurologo", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Object getAllNeurologos() {
		
		//Se crea una lista para asignar un seudonimo
		List<HashMap<String,Object>> objetos=new ArrayList<>();
		
		//Se crea una lista para almacenar los datos
		List<Neurologo> n=daoConsulta.getAllNeurologos();
		
		//Se genera un for para recorrer la lista
		for (Neurologo neurologo : n) {
			HashMap<String,Object> hm = new HashMap<>();//Se crea un nuevo objeto hashmap
			hm.put("id_neurologo",neurologo.getId_neurologo().toString());
			hm.put("nombre", neurologo.getNombre().toString());
			hm.put("direccion", neurologo.getDireccion().toString());
			hm.put("especialidad", neurologo.getEspecialidad().getEspecialidad());
			hm.put("operaciones","<button class='btn btn-primary ml-2 agregarNeu' data-dismiss='modal'>Agregar</button>");
			
			objetos.add(hm);
		}
		return Collections.singletonMap("data", objetos);
	}

	// metodo para traer los pacientes
	@GetMapping(value = "allPaciente", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Object getAllPacientes() {
		
		List<HashMap<String,Object>> objetos = new ArrayList<>();
		
		List<Paciente> p = daoConsulta.getAllPacientes();
		
		for (Paciente paciente : p) {
			HashMap<String,Object> hm = new HashMap<>();
			hm.put("id_paciente", paciente.getId_paciente());
			hm.put("nombre",paciente.getNombre());
			hm.put("direccion",paciente.getDireccion());
			hm.put("operaciones","<button class='btn btn-primary ml-2 agregarPaci' data-dismiss='modal'>Agregar</button>");
			
			objetos.add(hm);
		}
		return Collections.singletonMap("data", objetos);
	}

	// metodo para cargar modal update
	@GetMapping(value = "getConsulta/{id_consulta}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Consulta getConsulta(@PathVariable Integer id_consulta) {
		return daoConsulta.getConsulta(id_consulta);
	}

	// metodo para guardar
	@GetMapping(value = "save")
	@ResponseBody
	public HashMap<String, String> save(// declaramos los campos a sobreescribir
			@RequestParam Date fecha, @RequestParam String diagnostico, @RequestParam Neurologo id_neurologo,
			@RequestParam Paciente id_paciente) {
		Consulta c = new Consulta();// se crea un nuevo objeto

		HashMap<String, String> jsonReturn = new HashMap<>();
		// seteamos los campos a sobreescribir
		c.setFecha(fecha);
		c.setDiagnostico(diagnostico);
		c.setNeurologo(id_neurologo);
		c.setPaciente(id_paciente);
		try {// utlizamos try catch para facilitar ver los errores
			daoConsulta.SaveorUpdate(c);// utilizamos el objeto de service para implementar los metodos sobreescribir

			jsonReturn.put("estado", "OK");
			jsonReturn.put("mensaje", "Registro Guardado");

			return jsonReturn;
		} catch (Exception e) {
			jsonReturn.put("estado", "ERROR");
			jsonReturn.put("mensaje", "Registro no Guardado" + e.getMessage());

			return jsonReturn;
		}
	}

	// metodo para eliminar
	@GetMapping("delete/{id_consulta}")
	@ResponseBody
	public HashMap<String, String> delete(@PathVariable Integer id_consulta) {

		HashMap<String, String> jsonReturn = new HashMap<>();

		try {
			// buscar el registro
			Consulta c = daoConsulta.getConsulta(id_consulta);
			// eliminar el registro
			daoConsulta.delete(c);

			jsonReturn.put("estado", "OK");
			jsonReturn.put("mensaje", "Registro eliminado");

			return jsonReturn;
		} catch (Exception e) {
			jsonReturn.put("estado", "ERROR");
			jsonReturn.put("mensaje", "Registro no eliminado");

			return jsonReturn;
		}
	}

	// metodo para modificar
	@GetMapping("update/{id_consulta}")
	@ResponseBody
	public HashMap<String, String> update(@RequestParam Integer id_consulta, @RequestParam Date fecha,
			@RequestParam String diagnostico, @RequestParam Neurologo id_neurologo,
			@RequestParam Paciente id_paciente) {

		Consulta c = new Consulta();// se crea un nuevo objeto

		HashMap<String, String> jsonReturn = new HashMap<>();
		// seteamos los nuevos datos
		c.setId_consulta(id_consulta);
		c.setFecha(fecha);
		c.setDiagnostico(diagnostico);
		c.setNeurologo(id_neurologo);
		c.setPaciente(id_paciente);
		// manejando cualquier excepcion de error
		try {
			daoConsulta.SaveorUpdate(c);// se guarda el nuevo registro

			jsonReturn.put("estado", "OK");
			jsonReturn.put("mensaje", "Registro Actualizado");

			return jsonReturn;
		} catch (Exception e) {
			jsonReturn.put("estado", "ERROR");
			jsonReturn.put("mensaje", "Registro no actualizado" + e.getMessage());
			return jsonReturn;
		}
	}

}

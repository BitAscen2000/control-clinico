package com.cds.crud.controller;

import java.util.HashMap;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.cds.crud.entity.Especialidad;
import com.cds.crud.entity.Neurologo;
import com.cds.crud.service.NeurologoService;

@Controller
@CrossOrigin
@RequestMapping("neurologo")
public class NeurologoController {
	
	//repositorio para el manejo de datos
	@Autowired
	NeurologoService daoNeurologo;

	//metodo para llamar vista
	@GetMapping(value="index")
	public String neurologo() {
		return "views/neurologo/indexN";
	}
	
	//listar registros
	@GetMapping (value="all", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<Neurologo> getAllNeurologo(){
		return (List<Neurologo>) daoNeurologo.getAllNeurologos();
	}
	
	//listar registros de especialidades
	@GetMapping(value = "allEspecialidad", produces= MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<Especialidad> getAllEspecialidad(){
		return (List<Especialidad>) daoNeurologo.getAllEspecialidad();
	}
	
	
	//guardar
	@GetMapping(value ="save")
	@ResponseBody
	public HashMap<String,String> save(@RequestParam String direccion, @RequestParam String nombre,  @RequestParam Especialidad id) {
		Neurologo n = new Neurologo();//crear objeto de neurologo
		
		HashMap<String,String> jsonReturn = new HashMap<>();
	
		n.setNombre(nombre);
		n.setDireccion(direccion);
		n.setEspecialidad(id);
		//manejando cualquier excepcion de error
		try {
			daoNeurologo.SaveOrUpdate(n);//se guarda el registro del doctor
			
			jsonReturn.put("estado","OK");
			jsonReturn.put("mensaje","Registro Guardado");
			
			return jsonReturn;
		}catch(Exception e){
			jsonReturn.put("estado","ERROR");
			jsonReturn.put("mensaje","Registro no Guardado"+e.getMessage());
			
			return jsonReturn;
		}
	}
	
	//listar registro
	@GetMapping(value = "GetNeurologo/{id_neurologo}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Neurologo getNeurologo(@PathVariable Integer id_neurologo) {
		return daoNeurologo.getNeurologo(id_neurologo);
	} 

	
	//eliminar
		@GetMapping("delete/{id_neurologo}")
		@ResponseBody
		public HashMap<String,String> eliminar (@PathVariable Integer id_neurologo) {
			
			HashMap<String,String> jsonReturn = new HashMap<>();
			
			try {
				//buscar registro
				Neurologo n = daoNeurologo.getNeurologo(id_neurologo);
				daoNeurologo.delete(n);
				//elimina el registro
				daoNeurologo.delete(n);
				
				 jsonReturn.put("estado", "OK");
				 jsonReturn.put("mensaje", "Registro eliminado");
				 return jsonReturn;
			}catch(Exception e) {
				 jsonReturn.put("estado", "ERROR");
				 jsonReturn.put("mensaje", "Registro no eliminado"+e.getMessage());
				 return jsonReturn;
			}
		}
		
		//modificar
		@GetMapping(value ="update/{id_neurologo}")
		@ResponseBody
		public HashMap<String,String> update(
				@RequestParam Integer id_neurologo,
				@RequestParam String direccion, 
				@RequestParam String nombre,
				@RequestParam Especialidad id
				) {
			
			Neurologo n = new Neurologo();//se crea el objeto neurologo
			HashMap<String,String> jsonReturn = new HashMap<>();
			//asignando datos al objeto neurologo
			n.setId_neurologo(id_neurologo);
			n.setNombre(nombre);
			n.setDireccion(direccion);
			n.setEspecialidad(id);
			//manejando cualquier excepcion de error
			try {
				daoNeurologo.SaveOrUpdate(n);//se guarda el registro del doctor
				
				jsonReturn.put("estado","OK");
				jsonReturn.put("mensaje","Registro Actualizado");
				
				return jsonReturn;
			}catch(Exception e){
				jsonReturn.put("estado","ERROR");
				jsonReturn.put("mensaje","Registro no actualizado"+e.getMessage());
				return jsonReturn;
			}
		}
}
	

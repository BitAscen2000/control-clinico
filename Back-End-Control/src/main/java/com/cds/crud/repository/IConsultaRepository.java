package com.cds.crud.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.cds.crud.entity.Consulta;

@Repository
public interface IConsultaRepository extends CrudRepository<Consulta, Integer>{

}

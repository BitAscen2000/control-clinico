package com.cds.crud.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.cds.crud.entity.Neurologo;

@Repository
public interface INeurologoRepository extends CrudRepository<Neurologo,Integer>{

}
